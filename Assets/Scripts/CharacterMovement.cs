using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CharacterMovement : MonoBehaviour
{

    //UI display
    [SerializeField] Text TurnSpeedText ;
    [SerializeField] Text MovementSpeedText;


    //default speed
    int MovementSpeed = 2;
    int TurnSpeed = 30;


    [SerializeField]Animator CharacterAnimator;

    // Update is called once per frame  
    void Update()
    {
        if(TurnSpeedText.text != "")
        {
            TurnSpeed = int.Parse(TurnSpeedText.text);

            if (MovementSpeedText.text != "")
            {
                MovementSpeed = int.Parse(MovementSpeedText.text);

            }
        }
    

        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime* MovementSpeed);
            CharacterAnimator.SetBool("IsWalking", true);
        }
        else
        {
            CharacterAnimator.SetBool("IsWalking", false);

        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.Translate(Vector3.back * Time.deltaTime* MovementSpeed);
            CharacterAnimator.SetBool("IsWalking", true);

        }
   

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Rotate(Vector3.up, -TurnSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Rotate(Vector3.up, TurnSpeed * Time.deltaTime);
        }

    }
}
